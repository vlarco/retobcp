import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IAgency } from 'src/app/shared/models/agency';
import { HttpService } from '../http.service';

@Injectable({
  providedIn: 'root'
})
export class AgencyService {

  constructor(private http: HttpService) { }

  //create crud for agency
  getAgencies(): Observable<IAgency[]> {
    // no se utiliza los env vars porque aplica para el caso
    return this.http.get('http://localhost:4200/assets/agency.json');
  }

  // getAgency(id) {
  //   return this.http.get('/agencias/' + id);
  // }

  // createAgency(data) {
  //   return this.http.post('/agencias', data);
  // }

  // updateAgency(id, data) {
  //   return this.http.put('/agencias/' + id, data);
  // }

  // deleteAgency(id) {
  //   return this.http.delete('/agencias/' + id);
  // }

}
