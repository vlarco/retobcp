import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: HttpClient) {}

  get(url: string): Observable<any> {
    return this.http.get(url);
  }

  post(url: string, data: any, options: any): Observable<any> {
    return this.http.post(url, data, options);
  }

  put(url: string, data: any): Observable<any> {
    return this.http.put(url, data);
  }

  delete(url: string): Observable<any> {
    return this.http.delete(url);
  }

  patch(url: string, data: any): Observable<any> {
    return this.http.patch(url, data);
  }

  head(url: string): Observable<any> {
    return this.http.head(url);
  }

  options(url: string): Observable<any> {
    return this.http.options(url);
  }
}
