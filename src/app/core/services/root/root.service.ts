import { EventEmitter, Injectable } from '@angular/core';
import { SideNav, Header, Body, Theme } from 'src/app/shared/models/rootI';
import { IAgency } from 'src/app/shared/models/agency';
import { AgencyService } from '../agency/agency.service';

@Injectable({
  providedIn: 'root',
})
export class RootService {
  theme: EventEmitter<Theme> = new EventEmitter();
  sideNav: EventEmitter<SideNav> = new EventEmitter();
  header: EventEmitter<Header> = new EventEmitter();
  body: EventEmitter<Body> = new EventEmitter();

  constructor() {}

  init() {
    const theme = this.getLocalStorage('theme');
    const sideNav = this.getLocalStorage('sideNav');
    const header = this.getLocalStorage('header');
    const body = this.getLocalStorage('body');

    if (theme) {
      this.eventTheme(theme);
    } else {
      this.eventTheme({ theme: false });
    }

    if (sideNav) {
      this.eventSideNav(sideNav);
    } else {
      this.eventSideNav({ open: false });
    }

    if (header) {
      this.eventHeader(header);
    } else {
      this.eventHeader({ open: false });
    }

    if (body) {
      this.eventBody(body);
    } else {
      this.eventBody({ open: false });
    }
  }

  saveLocalStorage(key: string, value: any) {
    localStorage.setItem(key, JSON.stringify(value));
  }

  getLocalStorage(key: string) {
    return JSON.parse(localStorage.getItem(key) || '{}');
  }

  eventTheme(value: any) {
    this.saveLocalStorage('theme', value);
    this.theme.emit(value);
  }

  eventSideNav(value: any) {
    this.saveLocalStorage('sideNav', value);
    this.sideNav.emit(value);
  }

  eventHeader(value: any) {
    this.saveLocalStorage('header', value);
    this.header.emit(value);
  }

  eventBody(value: any) {
    this.saveLocalStorage('body', value);
    this.body.emit(value);
  }
  
}
