import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { SharedModule } from 'src/app/shared';
import { LoginRoutingModule } from './login.routing';

@NgModule({
  imports: [SharedModule, LoginRoutingModule],
  declarations: [LoginComponent],
})
export class LoginModule {}
