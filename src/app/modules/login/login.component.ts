import { AfterContentInit, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RootService } from 'src/app/core/services/root/root.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, AfterContentInit {
  login: FormGroup = this.formBuilder.group({
    email: ['test', Validators.required],
    password: ['test', Validators.required],
    remember_me: [false],
  });
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private rootService: RootService
  ) {}

  ngAfterContentInit() {
    this.logout();
  }

  ngOnInit() {}

  logout() {
    document.getElementsByTagName('html')[0].classList.remove('darkTheme');
    this.rootService.eventHeader({ open: false });
    this.rootService.eventSideNav({ open: false });
  }

  doLogin() {
    this.rootService.eventHeader({ open: true });
    this.rootService.eventSideNav({ open: true });
    this.router.navigate(['main']);
  }
}
