import { Component, OnInit } from '@angular/core';
import { delay } from 'rxjs';
import { AgencyService } from 'src/app/core/services/agency/agency.service';
import { RootService } from 'src/app/core/services/root/root.service';
import { IAgency } from 'src/app/shared/models/agency';

@Component({
  selector: 'app-agency',
  templateUrl: './agency.component.html',
  styleUrls: ['./agency.component.scss'],
})
export class AgencyComponent implements OnInit {
  agencies!: IAgency[];

  agency: any;

  showAgency = false;
  createAgency = false;

  objectKeys = Object.keys;

  list: any = {};

  constructor(private agencyService: AgencyService) {
    this.agencyService
      .getAgencies()
      .subscribe((data: IAgency[]) => {
        this.updateList(data);
      });
  }

  updateList(data: IAgency[]) {
    this.setLocalStorage(data);
    this.agencies = data;
    this.sourtByName();
    this.groupByFirstLetter();
  }

  setLocalStorage(c: IAgency[]) {
    localStorage.setItem('agency', JSON.stringify(c));
  }

  getLocalStorage() {
    this.agency = JSON.parse(localStorage.getItem('agency') || '{}');
  }

  sourtByName() {
    this.agencies.sort((a, b) => {
      if (a.agencia < b.agencia) {
        return -1;
      }
      if (a.agencia > b.agencia) {
        return 1;
      }
      return 0;
    });
  }

  groupByFirstLetter() {
    this.list = {};
    for (let index = 0; index < this.agencies.length; index++) {
      const element = this.agencies[index];
      const firstLetter = element.agencia.split('')[0];
      if (this.list[firstLetter]) {
        this.list[firstLetter].push({
          agencia: element.agencia,
          distrito: element.distrito,
          provincia: element.provincia,
          departamento: element.departamento,
          direccion: element.direccion,
          lat: element.lat,
          lon: element.lon,
        });
      } else {
        this.list[firstLetter] = [
          {
            agencia: element.agencia,
            distrito: element.distrito,
            provincia: element.provincia,
            departamento: element.departamento,
            direccion: element.direccion,
            lat: element.lat,
            lon: element.lon,
          },
        ];
      }
    }
  }

  ngOnInit() {}

  getAvatar(agency: string) {
    return agency.split('')[0];
  }

  toggleAgency(c: any) {
    this.agency = c;
    this.showAgency = true;
    this.createAgency = false;
  }

  toggleCreateAgency() {
    this.showAgency = false;
    this.createAgency = true;
  }

  editAgencyAction(c: any) {
    this.agencies.map((item: IAgency) => {
      if (item.agencia === this.agency.agencia) {
        item.agencia = c.agencia;
        item.distrito = c.distrito;
        item.provincia = c.provincia;
        item.departamento = c.departamento;
        item.direccion = c.direccion;
        item.lat = c.lat;
        item.lon = c.lon;
      }
    });
    this.updateList(this.agencies);
    this.toggleAgency(c);
  }
}
