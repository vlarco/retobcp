import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { IAgency } from 'src/app/shared/models/agency';

@Component({
  selector: 'app-showAgency',
  templateUrl: './showAgency.component.html',
  styleUrls: ['./showAgency.component.scss'],
})
export class ShowAgencyComponent implements OnInit, OnChanges {
  @Input()
  agency!: IAgency;
  @Output() closeCard = new EventEmitter<boolean>();
  @Output() editAgency = new EventEmitter<IAgency>();

  center = { lat: 24, lng: 12 };
  zoom = 7;
  display?: google.maps.LatLngLiteral;

  constructor() {}

  ngOnChanges(changes: SimpleChanges): void {
    this.center.lat = this.agency?.lat || 24;
    this.center.lng = this.agency?.lon || 12;
  }

  ngOnInit() {}

  close() {
    this.closeCard.emit(false);
  }

  edit() {
    this.editAgency.emit(this.agency);
  }
}
