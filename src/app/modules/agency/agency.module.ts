import { NgModule } from '@angular/core';
import { AgencyComponent } from './agency.component';
import { SharedModule } from 'src/app/shared';
import { AgencyRoutingModule } from './agency.routing';
import { ShowAgencyComponent } from './showAgency/showAgency.component';
import { CreateAgencyComponent } from './createAgency/createAgency.component';

@NgModule({
  imports: [SharedModule, AgencyRoutingModule],
  declarations: [
    AgencyComponent,
    ShowAgencyComponent,
    CreateAgencyComponent,
  ],
})
export class AgencyModule {}
