import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RootService } from 'src/app/core/services/root/root.service';
import { IAgency } from 'src/app/shared/models/agency';

@Component({
  selector: 'app-createAgency',
  templateUrl: './createAgency.component.html',
  styleUrls: ['./createAgency.component.scss'],
})
export class CreateAgencyComponent implements OnInit, OnChanges {
  @Input() agency!: IAgency;
  @Output() closeCard = new EventEmitter<IAgency>();

  agencyForm: FormGroup = this.formBuilder.group({
    agencia: ['', Validators.required],
    distrito: ['', Validators.required],
    provincia: ['', Validators.required],
    departamento: ['', Validators.required],
    direccion: ['', Validators.required],
    lat: ['', Validators.required],
    lon: ['', Validators.required],
  });

  constructor(private formBuilder: FormBuilder, private rootService: RootService) {}

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges): void {
    if (this.agency?.agencia) this.agencyForm.setValue(this.agency);
  }

  saveAgency() {
    this.closeCard.emit(this.agencyForm.value);
  }
}
