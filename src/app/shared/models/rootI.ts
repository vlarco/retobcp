export interface Theme {
  theme: boolean;
}

export interface SideNav {
  open: boolean;
}

export interface Body {
  open: boolean;
}

export interface Header {
  open: boolean;
}
