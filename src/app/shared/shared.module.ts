import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localeEsPe from '@angular/common/locales/es-PE';

import { MaterialModule } from './material.module';
import { MatNativeDateModule } from '@angular/material/core';
import { FooterComponent, HeaderComponent, TableComponent } from '.';
import { SidenavComponent } from './components/sidenav/sidenav.component';

import { GoogleMapsModule } from '@angular/google-maps';

registerLocaleData(localeEsPe, 'es-Pe');

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    MaterialModule,
    MatNativeDateModule,
    GoogleMapsModule,
  ],
  declarations: [
    FooterComponent,
    HeaderComponent,
    TableComponent,
    SidenavComponent,
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    TableComponent,
    SidenavComponent,
    CommonModule,
    GoogleMapsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule,
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'es-Pe' }],
})
export class SharedModule {}
