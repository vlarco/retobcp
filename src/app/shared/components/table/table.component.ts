import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  @Input() tableInfo: any;
  Tcolumns: any = [];
  displayedColumns: any = [];
  dataSource: any = [];

  constructor() {}

  ngOnInit() {
    this.Tcolumns = this.tableInfo.columns;
    this.dataSource = this.tableInfo.data;
    this.Tcolumns.forEach((column: any) => {
      this.displayedColumns.push(column.key);
    });
  }
}
