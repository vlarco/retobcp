import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { RootService } from 'src/app/core/services/root/root.service';
import { SideNav, Theme } from '../../models/rootI';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  sideNav: boolean = true;
  theme: boolean = false;

  constructor(
    private overlay: OverlayContainer,
    private rootService: RootService
  ) {
    this.rootService.sideNav.subscribe((value: SideNav) => {
      this.sideNav = value.open;
    });
    this.rootService.theme.subscribe((value: Theme) => {
      this.theme = value.theme;
    });
  }

  ngOnInit() {}

  toggleSideNav() {
    this.sideNav = this.sideNav ? false : true;
    this.rootService.eventSideNav({ open: this.sideNav });
  }

  toggleFullScreen() {
    if (!document.fullscreenElement) {
      document.documentElement.requestFullscreen();
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      }
    }
  }

  toggleTheme() {
    this.theme = this.theme ? false : true;
    this.rootService.eventTheme({ theme: this.theme });
    if (this.theme) {
      this.overlay.getContainerElement().classList.add('darkTheme');
    } else {
      this.overlay.getContainerElement().classList.remove('darkTheme');
    }
  }
}
