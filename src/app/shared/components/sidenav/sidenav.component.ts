import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RootService } from 'src/app/core/services/root/root.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
})
export class SidenavComponent implements OnInit {
  options = [
    {
      title: 'Opciones',
      menu: [
        {
          path: 'main/agency',
          icon: 'store',
          name: 'Agencias',
        },
      ],
    },
  ];
  constructor(private router: Router, private rootService: RootService) {}

  ngOnInit() {}

  logout() {
    this.rootService.eventHeader({ open: false });
    this.rootService.eventSideNav({ open: false });
    this.router.navigate(['/login']);
  }
}
