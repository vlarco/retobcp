import {
  AfterContentInit,
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
} from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { RootService } from './core/services/root/root.service';
import { Header, SideNav, Theme } from './shared/models/rootI';
import { LoadingService } from './core/services/loading/loadingService.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterContentInit, OnDestroy {
  sideNav!: boolean;
  header!: boolean;
  mobileQuery!: MediaQueryList;
  private _mobileQueryListener: () => void;

  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private rootService: RootService,
    public loadingService: LoadingService
  ) {
    this.rootService.theme.subscribe((value: Theme) => {
      if (value.theme) {
        document.getElementsByTagName('html')[0].classList.add('darkTheme');
      } else {
        document.getElementsByTagName('html')[0].classList.remove('darkTheme');
      }
    });
    this.rootService.sideNav.subscribe((value: SideNav) => {
      this.sideNav = value.open;
    });
    this.rootService.header.subscribe((value: Header) => {
      this.header = value.open;
    });
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this._mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addListener(this._mobileQueryListener);
  }

  ngAfterContentInit(): void {
    this.rootService.init();
  }

  ngOnDestroy() {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  ngOnInit() {}
}
